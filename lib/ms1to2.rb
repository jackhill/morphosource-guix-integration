module Ms1to2
	extend ActiveSupport::Autoload
	autoload :CSVParser
	autoload :Importer
	autoload :Normalizer
	autoload :Ms1InputData
	autoload :Ms2OutputData
end