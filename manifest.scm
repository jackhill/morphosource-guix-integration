(use-modules
 (gnu packages)
 (guix channels)
 (guix inferior)
 (srfi srfi-1))

(define standard-packages
  (let ((lookup-package (lambda (name) (specification->package name))))
    (map lookup-package '("glibc-locales"
			  "nss-certs"))))

(define blender-commit "26986544469ef290885f5f8d71006751e9e8daf8")
(define dcmtk-commit "26986544469ef290885f5f8d71006751e9e8daf8")

(define (channels-for-commit hash)
  (list (channel
	 (name 'guix)
	 (url "https://git.savannah.gnu.org/git/guix.git")
	 (commit hash))))

(define (inferior-package name hash)
  (let* ((channel (channels-for-commit hash))
	 (inferior (inferior-for-channels channel)))
    (first (lookup-inferior-packages inferior name))))

(packages->manifest
 (cons* (inferior-package "blender" blender-commit)
	(inferior-package "dcmtk" dcmtk-commit)
	standard-packages))
