# Generated via
#  `rails generate hyrax:work Attachment`
module Hyrax
  class AttachmentPresenter < Hyrax::WorkShowPresenter
    include Morphosource::PresenterMethods
  end
end
